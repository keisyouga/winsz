#include <tcl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static int WinszCmd(ClientData clientData,
                    Tcl_Interp *interp, int objc, Tcl_Obj *const objv[])
{
	int command;
	struct winsize ws = {0};
	int fd = STDOUT_FILENO;
	char *ttyname = NULL;   // ttyname to open
	int wsindex;    // position of row in objv

	if (objc < 2) {
		Tcl_WrongNumArgs(interp, 1, objv, "command ?...?");
		return TCL_ERROR;
	}

	static const char *const commands[] = {"get", "set", NULL};
	enum winszCommands {CMD_GET, CMD_SET};
	if (Tcl_GetIndexFromObj(interp, objv[1], commands, "command", 0,
	                        &command) != TCL_OK) {
		return TCL_ERROR;
	}

	switch (command) {
	case CMD_GET:
		// set ttyname
		// default is /dev/tty
		if ((objc == 4) && (strcmp(Tcl_GetString(objv[2]), "-tty") == 0)) {
			ttyname = Tcl_GetString(objv[3]);
		} else if (objc == 2) {
			ttyname = "/dev/tty";
		} else {
			Tcl_WrongNumArgs(interp, 1, objv, "get ?-tty tty?");
			return TCL_ERROR;
		}

		fd = open(ttyname, O_RDONLY);
		if (fd == -1) {
			perror(ttyname);
			return TCL_ERROR;
		}

		// get window size
		if (ioctl(fd, TIOCGWINSZ, &ws) == -1) {
			perror("ioctl TIOCGWINSZ");
			return TCL_ERROR;
		}

		Tcl_Obj *resultPtr = Tcl_NewObj();
		Tcl_ListObjAppendElement(NULL, resultPtr, Tcl_NewIntObj(ws.ws_row));
		Tcl_ListObjAppendElement(NULL, resultPtr, Tcl_NewIntObj(ws.ws_col));
		Tcl_ListObjAppendElement(NULL, resultPtr, Tcl_NewIntObj(ws.ws_xpixel));
		Tcl_ListObjAppendElement(NULL, resultPtr, Tcl_NewIntObj(ws.ws_ypixel));
		Tcl_SetObjResult(interp, resultPtr);

		close(fd);

		break;
	case CMD_SET:
		// set ttyname
		// default is /dev/tty
		if ((objc == 8) && (strcmp(Tcl_GetString(objv[2]), "-tty") == 0)) {
			ttyname = Tcl_GetString(objv[3]);
			wsindex = 4;
		} else if (objc == 6) {
			ttyname = "/dev/tty";
			wsindex = 2;
		} else {
			Tcl_WrongNumArgs(interp, 1, objv, "set ?-tty tty? row col xpixel ypixel");
			return TCL_ERROR;
		}

		fd = open(ttyname, O_RDONLY);
		if (fd == -1) {
			perror(ttyname);
			return TCL_ERROR;
		}

		int row, col, xpixel, ypixel;
		Tcl_GetIntFromObj(interp, objv[wsindex], &row);
		Tcl_GetIntFromObj(interp, objv[wsindex + 1], &col);
		Tcl_GetIntFromObj(interp, objv[wsindex + 2], &xpixel);
		Tcl_GetIntFromObj(interp, objv[wsindex + 3], &ypixel);
		ws.ws_row = row;
		ws.ws_col = col;
		ws.ws_xpixel = xpixel;
		ws.ws_ypixel = ypixel;

		// set window size
		if (ioctl(fd, TIOCSWINSZ, &ws) == -1) {
			perror("ioctl TIOCSWINSZ");
			return TCL_OK;
		}

		close(fd);

		break;
	}

	return TCL_OK;
}

int Winsz_Unload(Tcl_Interp *interp, int flags)
{
	Tcl_DeleteCommand(interp, "winsz");
	return TCL_OK;
}

int Winsz_Init(Tcl_Interp *interp)
{
	if (Tcl_InitStubs(interp, TCL_VERSION, 0) == NULL) {
		return TCL_ERROR;
	}
	if (Tcl_PkgProvide(interp, "winsz", "1.0.0") == TCL_ERROR) {
		return TCL_ERROR;
	}

	Tcl_CreateObjCommand(interp, "winsz", WinszCmd, NULL, NULL);

	return TCL_OK;
}
