PROGRAM = winsz
CFLAGS = -Wall -DUSE_TCL_STUBS `pkg-config --cflags tcl` -fPIC
LDFLAGS = -shared -o lib$(PROGRAM).so
LDLIBS = `pkg-config --libs tcl`
OBJS = winsz.o

lib$(PROGRAM).so: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) $(LDLIBS)
